//
//  PulentoTestTests.swift
//  PulentoTestTests
//
//  Created by BrUjO on 30-04-20.
//  Copyright © 2020 BrUjO. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import PulentoTest

class PulentoTestTests: XCTestCase {
    
    //Custom test default max results (100) and first page (20)
    func testPageSongs() {
         let numberOfPageSongs = 20
         let textToSearch = "Slayer"
         let searchSongPresenter = SearchSongsPresenter()
        
         let testPageSongsExp = expectation(description: "testPageSongs")

        testPageSongsNumberWith(searchTerm: textToSearch , presenter: searchSongPresenter, time: 1.0)
            {
                songsCount in
                print(" Page Songs Count for: \(textToSearch) is \(songsCount)")
              XCTAssertEqual(songsCount, numberOfPageSongs)
              testPageSongsExp.fulfill()
          }
          wait(for: [testPageSongsExp], timeout: 3.0)
      }
    
    
    // Test with text and maxResultsLimit as parameters
    func testSearchSongsCase1() throws{
        let term = ""
        let maxResultsLimit = 100
        
        try searchSongWithTerm(searchTerm: term, maxResultsLimit: maxResultsLimit)
       }
    
    func testSearchSongsCase2() throws{
        let term = "Metallica"
        let maxResultsLimit = 30
        
        try searchSongWithTerm(searchTerm: term, maxResultsLimit: maxResultsLimit)
       }
    
    func testSearchSongsCase3() throws{
        let term = "Metallica"
        let maxResultsLimit = 100
        
        try searchSongWithTerm(searchTerm: term, maxResultsLimit: maxResultsLimit)
       }
    
    
    func testSearchSongsCase4() throws{
        let term = "_923829_ñ"
        let maxResultsLimit = 30
        
        try searchSongWithTerm(searchTerm: term, maxResultsLimit: maxResultsLimit)
       }
    
    //Using Stub with 0 results
    func testSearchSongsCase5() throws{
            stub(condition: isHost("itunes.apple.com")) { _ in
                  let stubPath = OHPathForFile("SongSearchResult0.json", type(of: self))
                return fixture(filePath: stubPath!,status: 200, headers: ["Content-Type":"application/json"])
                }
        let term = "Metallica"
        let maxResultsLimit = 100
        let expectedResults = 0
        
        try searchSongWithTerm(searchTerm: term, maxResultsLimit: maxResultsLimit, expectedResults: expectedResults)
       }
    
    //Using Stub with 15 results
    func testSearchSongsCase6() throws{
            stub(condition: isHost("itunes.apple.com")) { _ in
                  let stubPath = OHPathForFile("SongSearchResult15.json", type(of: self))
                return fixture(filePath: stubPath!,status: 200, headers: ["Content-Type":"application/json"])
                }
        let term = "Metallica"
        let expectedResults = 15
        let maxResultsLimit = 100
        
        try searchSongWithTerm(searchTerm: term, maxResultsLimit: maxResultsLimit, expectedResults: expectedResults)
       }
    
    //Using Stub with 30 results
    func testSearchSongsCase7() throws{
            stub(condition: isHost("itunes.apple.com")) { _ in
                  let stubPath = OHPathForFile("SongSearchResult30.json", type(of: self))
                return fixture(filePath: stubPath!,status: 200, headers: ["Content-Type":"application/json"])
                }
        let term = "Metallica"
        let expectedResults = 30
        let maxResultsLimit = 100
        
        try searchSongWithTerm(searchTerm: term, maxResultsLimit: maxResultsLimit, expectedResults: expectedResults)
       }

    func searchSongWithTerm(searchTerm:String ,  maxResultsLimit: Int, expectedResults: Int? = nil) throws {
        let searchSongPresenter = SearchSongsPresenter()
        
        // create the expectation
        let searchSongsExpectation = expectation(description: "Loading song list")
        var songList = [Song]()
    
        searchSongPresenter.callSongsServiceForTerm(searchTerm: searchTerm, maxLimit: maxResultsLimit){
            response in
            songList = response!
            print("***  Number of results: \(songList.count)")
            searchSongsExpectation.fulfill()
        }
        wait(for: [searchSongsExpectation], timeout: 3.0)
        
        guard !songList.isEmpty else {
            XCTAssertEqual(songList.count, 0, "No search Results.")
            return
        }
        
        if (expectedResults != nil) {
        XCTAssertEqual(songList.count, expectedResults!, "We should have loaded exactly the song results amount as expectes results.")
        }else{
        XCTAssertEqual(songList.count, maxResultsLimit, "We should have loaded exactly the song results amount as limit.")
        }
    }
    
    func testPageSongsNumberWith(searchTerm:String, presenter: SearchSongsPresenter, time: Double, completion: @escaping (Int)->()) {
        presenter.searchSongsFor(searchTerm: searchTerm)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + time) {
            let songsCount = presenter.getSongCount()
            completion (songsCount)
        }
    }
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

//    func testPerformanceExample() throws {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }

}
