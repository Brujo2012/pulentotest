//
//  Song.swift
//  PulentoTest
//
//  Created by BrUjO on 28-04-20.
//  Copyright © 2020 BrUjO. All rights reserved.
//

import Foundation

struct Song {
    let artistId: Int?
    let collectionId: Int?
    let trackId: Int?
    let artistName: String?
    let collectionName: String?
    let trackName: String?
    let collectionCensoredName: String?
    let trackCensoredName: String?
    let artistViewUrl: String?
    let collectionViewUrl: String?
    let trackViewUrl: String?
    let previewUrl: String?
    let artworkUrl30: String?
    let artworkUrl60: String?
    let artworkUrl100: String?
    let collectionPrice: Double?
    let trackPrice: Double?
    let collectionHdPrice: Double?
    let trackHdPrice: Double?
    let releaseDate: String?
    let currency: String?
    let shortDescription: String?
}


