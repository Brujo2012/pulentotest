//
//  DetailVCPresenter.swift
//  PulentoTest
//
//  Created Mario Alejandro on 01-05-20.
//  Copyright © 2020 BrUjO. All rights reserved.
//
//  Template generated by Edward
//

import Foundation

// MARK: View -
protocol DetailVCViewProtocol: class {
    func refreshSongs()
    func showLoading()
    func hideLoading()
    func showAlertWith(title: String, message: String)
}

// MARK: Presenter -
protocol DetailVCPresenterProtocol: class {
	var view: DetailVCViewProtocol? { get set }
    var collectionId : Int {get set}
    var albumbSong : Song? {get set}

    func viewDidLoad()
    func viewWillAppear()
    func searchSongsForAlbum(collectionId: Int)
    func getSong(atIndex: IndexPath) -> Song
    func getSongCount() -> Int
    func getAlbumData() -> Song
}

class DetailVCPresenter: DetailVCPresenterProtocol {
    var albumbSong : Song?
    
    func getAlbumData() -> Song {
        return albumbSong!
    }
    

    var collectionId: Int = 0
    weak var view: DetailVCViewProtocol?
    var songsList =  [Song]()
    func viewDidLoad() {
      
    }
    
    func viewWillAppear(){
        searchSongsForAlbum(collectionId: collectionId)
    }
    
    func getSong(atIndex: IndexPath) -> Song {
          return songsList[atIndex.row]
      }
      
      func getSongCount() -> Int {
          songsList.count
      }
      
      func didLoadSongs(songsList: [Song]){
          self.view?.hideLoading()
          self.songsList = songsList
          self.view?.refreshSongs()
      }
    
    func searchSongsForAlbum(collectionId: Int) {
        self.view?.showLoading()
        guard Connectivity.isConnectedToInternet else {
            print("**** Offline ***")
            self.view!.showAlertWith(title: "Sin conexión", message: "Se requiere una conexión para usar esta app.")
                   self.view?.hideLoading()
            return
        }
        ITunesServiceManager.init().getSongListFromAlbum(collectionId:collectionId ){
            response in
         self.songsList = response!
         self.didLoadSongs(songsList: self.songsList)
           
        }
    }
}
