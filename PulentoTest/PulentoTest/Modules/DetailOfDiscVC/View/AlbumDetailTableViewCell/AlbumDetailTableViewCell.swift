//
//  AlbumDetailTableViewCell.swift
//  PulentoTest
//
//  Created by Mario Alejandro on 02-05-20.
//  Copyright © 2020 BrUjO. All rights reserved.
//

import UIKit

class AlbumDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
         
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
