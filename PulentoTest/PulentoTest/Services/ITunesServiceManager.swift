//
//  ServiceManager.swift
//  PulentoTest
//
//  Created by BrUjO on 30-04-20.
//  Copyright © 2020 BrUjO. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import SwiftyJSON
import OHHTTPStubs

class ITunesServiceManager: NSObject {
    
    override init() {
     super.init()
    //Uncomment one of the next examples to test and simulate in UI with HTTPStubs our 4 cases defined as extension at the end of this class:
        
//     simulateStubResults(simulatedNumber: .results0)
//     simulateStubResults(simulatedNumber: .results15)
//     simulateStubResults(simulatedNumber: .results30)
//     simulateStubResults(simulatedNumber: .results101)
    }
    
    func getSongList(searchTerm:String, maxLimit: Int, completion: @escaping ([Song]?) -> ())  {
        var songsList = [Song]()
        let url =  "https://itunes.apple.com/search?term="+searchTerm+"&mediaType=music&limit=\(maxLimit)"
        
        AF.request(url).responseJSON {  (responseData) -> Void in
            if((responseData.data) != nil) {
                let swiftyJsonVar = JSON(responseData.data!)
                let jsonList = swiftyJsonVar["results"]
                for (_, dict) in jsonList{
                    let thisObject = Song(artistId: dict["artistId"].intValue, collectionId: dict["collectionId"].intValue, trackId: dict["trackId"].intValue, artistName: dict["artistName"].stringValue, collectionName: dict["collectionName"].stringValue, trackName: dict["trackName"].stringValue, collectionCensoredName: dict["collectionCensoredName"].stringValue, trackCensoredName: dict["trackCensoredName"].stringValue, artistViewUrl: dict["artistViewUrl"].stringValue, collectionViewUrl: dict["collectionViewUrl"].stringValue, trackViewUrl: dict["trackViewUrl"].stringValue, previewUrl: dict["previewUrl"].stringValue, artworkUrl30: dict["artworkUrl30"].stringValue, artworkUrl60: dict["artworkUrl60"].stringValue, artworkUrl100: dict["artworkUrl100"].stringValue, collectionPrice: dict["collectionPrice"].doubleValue, trackPrice: dict["trackPrice"].doubleValue, collectionHdPrice: dict["collectionHdPrice"].doubleValue, trackHdPrice: dict["trackHdPrice"].doubleValue, releaseDate: dict["releaseDate"].stringValue, currency: dict["currency"].stringValue, shortDescription: dict["shortDescriprion"].stringValue)
                    songsList.append(thisObject)
                }
                print("** Total number of Songs: \(songsList.count)")
                completion(songsList)

            }else{
                completion(songsList)
            }

        }

    }
    
    func getSongListFromAlbum(collectionId: Int, completion: @escaping ([Song]?) -> ())  {
        var songsList = [Song]()
        let url =  "https://itunes.apple.com/lookup?id=\(collectionId)&entity=song"
        AF.request(url).responseJSON {  (responseData) -> Void in
            if((responseData.data) != nil) {
                let swiftyJsonVar = JSON(responseData.data!)
                let jsonList = swiftyJsonVar["results"]
                for (_, dict) in jsonList{
                    let thisObject = Song(artistId: dict["artistId"].intValue, collectionId: dict["collectionId"].intValue, trackId: dict["trackId"].intValue, artistName: dict["artistName"].stringValue, collectionName: dict["collectionName"].stringValue, trackName: dict["trackName"].stringValue, collectionCensoredName: dict["collectionCensoredName"].stringValue, trackCensoredName: dict["trackCensoredName"].stringValue, artistViewUrl: dict["artistViewUrl"].stringValue, collectionViewUrl: dict["collectionViewUrl"].stringValue, trackViewUrl: dict["trackViewUrl"].stringValue, previewUrl: dict["previewUrl"].stringValue, artworkUrl30: dict["artworkUrl30"].stringValue, artworkUrl60: dict["artworkUrl60"].stringValue, artworkUrl100: dict["artworkUrl100"].stringValue, collectionPrice: dict["collectionPrice"].doubleValue, trackPrice: dict["trackPrice"].doubleValue, collectionHdPrice: dict["collectionHdPrice"].doubleValue, trackHdPrice: dict["trackHdPrice"].doubleValue, releaseDate: dict["releaseDate"].stringValue, currency: dict["currency"].stringValue, shortDescription: dict["trackNumber"].stringValue)
                    songsList.append(thisObject)
                }
                if(!songsList.isEmpty){                    songsList.removeFirst()
                }
                completion(songsList)

            }else{
                 if(!songsList.isEmpty){                    songsList.removeFirst()
                              }
                completion(songsList)
            }

        }

    }

}


extension ITunesServiceManager {
    
    enum SimulatedResultsNumber {
        case results0
        case results15
        case results30
        case results101
        case none
    }
    
    /// HTTPStubs simulation of server results for test
    /// - Parameter simulatedNumber: an enum type SimulatedResultsNumber
    func simulateStubResults(simulatedNumber: SimulatedResultsNumber){
        
        let simulatedResults = simulatedNumber
               
        var stubJson: String = ""
               
               switch simulatedResults {
               case .results0:
                   stubJson = "SongSearchResult0.json"
               case .results15:
                   stubJson = "SongSearchResult15.json"
               case .results30:
                   stubJson = "SongSearchResult30.json"
               case .results101:
                   stubJson = "SongSearchResult101.json"
               case .none:
                   print("none")
               }
                           
        stub(condition: isHost("itunes.apple.com")) { _ in
        let stubPath = OHPathForFile(stubJson, type(of: self))
            return fixture(filePath: stubPath!,status: 200, headers: ["Content-Type":"application/json"])
            }
    }
    
    
}
