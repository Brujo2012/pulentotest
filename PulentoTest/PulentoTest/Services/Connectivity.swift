//
//  Connectivity.swift
//  PulentoTest
//
//  Created by BrUjO on 02-05-20.
//  Copyright © 2020 BrUjO. All rights reserved.
//

import Foundation
import Alamofire

class Connectivity {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
